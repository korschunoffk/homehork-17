# Описание
1. inetRouter1 192.168.255.2
2. inetRouter2 192.168.254.2 (с хоста 192.168.11.101)
3. centralRouter 192.168.255.2 192.168.254.2 192.168.0.1
4. centralServer 192.168.0.2


# Port Knocking

port  knoking реализован при помощи демона knockd

на inetRouter1 установлен и сконфигурирован knockd:

`/etc/knockd.conf`

```
[options]
            logfile = /var/log/knockd.log
            interface = eth1                            # меняем интерфейс для прослушивания

[openSSH]
        sequence = 5040,6010,6500                       # устанавливаем порты для стучания
        seq_timeout = 30                                
        tcpflags = syn
        Start_command = /sbin/iptables -I INPUT -s %IP% -p tcp --dport 22 -j ACCEPT       # если все ок, открываем порт 22 для стучащего ip

[closeSSH]
        sequence = 4040,5050,8080
        seq_timeout = 30
        command = /sbin/iptables -I INPUT -s %IP% -p tcp --dport 22 -j ACCEPT
        tcpflags = syn
```
Включаем аутентификацию по паролю  и запрещающее правило для подключения по 22 порту

```
sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config

iptables -A INPUT -p tcp --dport 22 -j DROP
```

На centralRouter 
Устанавливаем "стучалку"
`yum install nmap -y`
и создаем скрипт, который будет располагаться в `/opt/knock.sh`

```
#!/bin/bash
HOST=\$1 
shift
for ARG in "\$@"
do
  nmap -Pn --host-timeout 100 --max-retries 0 -p \$ARG \$HOST
done
```
Стучимся 

`bash knock.sh 192.168.255.1 5040 6010 6500`

и подключаемся 

`ssh vagrant@192.168.255.1`

# проброс nginx 

Nginx устанавливаем на `centralServer (192.168.0.2)`
```
yum install epel-release 
yum install nginx 
systemctl start nginx
```

Конфигурируем inetRouter2


```
sysctl -w net.ipv4.ip_forward=1                                                                 #вкл форвардинг
iptables -t nat -A PREROUTING -p tcp --dport 8080 -j DNAT --to-destination 192.168.0.2:80       #Натим все входящие запросы с 8080 порта на 192.168.0.2:80 где живет nginx
iptables -t nat -A POSTROUTING -j MASQUERADE                                                    #маскарадим все пакеты(подменяем адрес отправителя, что бы обратные пакеты дошли до inetRouter2)
service iptables save                                                                               
ip route add 192.168.0.0/24 via 192.168.254.2                                                   #добавляем маршрут для 192.168.0.0 что бы отправлять пакеты на centralRouter, который уже доставит их до nginx
```

Проверить работоспособность стенда можно с хоста командой `curl http://192.168.11.101:8080`

```
Обратно от nginx пакеты идут так же через inetRouter2, тк nginx отсылает пакеты на 192.168.254.1,
а для 192.168.254.1 есть автоматически созданный маршрут на centralRouter.
```





