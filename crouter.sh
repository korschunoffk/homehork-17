#!/bin/bash
echo "DEFROUTE=no" >> /etc/sysconfig/network-scripts/ifcfg-eth0 
echo "GATEWAY=192.168.255.1" >> /etc/sysconfig/network-scripts/ifcfg-eth1
systemctl restart network

yum install nmap -y
sysctl -w net.ipv4.ip_forward=1
touch /opt/knock.sh
chmod +x /opt/knock.sh

cat <<EOT>> /opt/knock.sh
            
#!/bin/bash
HOST=\$1 
shift
for ARG in "\$@"
do
  nmap -Pn --host-timeout 100 --max-retries 0 -p \$ARG \$HOST
done

EOT

ip route delete default
ip route add 0.0.0.0/0 via 192.168.255.1
