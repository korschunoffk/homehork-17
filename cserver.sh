echo "DEFROUTE=no" >> /etc/sysconfig/network-scripts/ifcfg-eth0 
echo "GATEWAY=192.168.0.1" >> /etc/sysconfig/network-scripts/ifcfg-eth1
ip route del default
systemctl restart network
ip route add 0.0.0.0/0 via 192.168.0.1

yum install epel-release -y
yum install nginx -y
systemctl start nginx
