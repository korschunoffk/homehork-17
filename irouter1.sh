#!/bin/bash

iptables -t nat -A POSTROUTING ! -d 192.168.0.0/16 -o eth0 -j MASQUERADE
ip route add 192.168.0.0/22 via 192.168.255.2 

yum install libpcap-devel -y

touch /etc/yum.repos.d/nux-misc.repo

cat <<EOT>> /etc/yum.repos.d/nux-misc.repo
[nux-misc]
name=Nux Misc
baseurl=http://li.nux.ro/download/nux/misc/el6/x86_64/
enabled=0
gpgcheck=1
gpgkey=http://li.nux.ro/download/nux/RPM-GPG-KEY-nux.ro

EOT

yum --enablerepo=nux-misc install knock-server -y

> /etc/knockd.conf

cat <<EOT>> /etc/knockd.conf

[options]
        logfile = /var/log/knockd.log
        interface = eth1

[openSSH]
        sequence = 5040,6010,6500
        seq_timeout = 30
        tcpflags = syn
        Start_command = /sbin/iptables -I INPUT -s %IP% -p tcp --dport 22 -j ACCEPT

[closeSSH]
        sequence = 4040,5050,8080
        seq_timeout = 30
        command = /sbin/iptables -I INPUT -s %IP% -p tcp --dport 22 -j ACCEPT
        tcpflags = syn

EOT
service knockd  start
sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config
service sshd restart
sysctl -w net.ipv4.ip_forward=1
iptables -A INPUT -p tcp --dport 22 -j DROP