#!/bin/bash
sysctl -w net.ipv4.ip_forward=1
iptables -t nat -A PREROUTING -p tcp --dport 8080 -j DNAT --to-destination 192.168.0.2:80
iptables -t nat -A POSTROUTING -j MASQUERADE
service iptables save
ip route add 192.168.0.0/24 via 192.168.254.2